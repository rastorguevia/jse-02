package ru.rastorguev.tm;

import ru.rastorguev.tm.controller.CommandController;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Application {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        CommandController commandController = new CommandController(reader);

        System.out.println("* TASK MANAGER *");
        System.out.println(" help - show all commands.");

        while(true){
            String input = reader.readLine();

            if (input.equals("exit")) {
                reader.close();
                System.exit(0);
            }
            commandController.commandExecution(input);
        }
    }
}