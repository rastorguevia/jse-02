package ru.rastorguev.tm.controller;

import java.io.BufferedReader;
import java.io.IOException;

public class CommandController {
    private BufferedReader reader;
    private ProjectController projectController = new ProjectController();
    private TaskController taskController = new TaskController();

    public CommandController(BufferedReader reader) {
        this.reader = reader;
    }

    public void commandExecution (String input) throws IOException {
        switch (input) {
            case "help":
                System.out.println("project-create: Create new project.\n" +
                        "project-list: Show all projects.\n" +
                        "project-edit: Edit selected project\n" +
                        "project-remove: Remove selected project\n" +
                        "project-clear: Remove all projects.\n\n" +
                        "task-create: Create new task.\n" +
                        "task-list: Show all tasks.\n" +
                        "task-edit: Edit selected task.\n" +
                        "task-remove: Remove selected task.\n" +
                        "task-clear: Remove all tasks.");
                break;
            case "project-create":
                projectController.createProject(reader);
                break;
            case "project-list":
                projectController.listProjects(reader);
                break;
            case "project-edit":
                projectController.editProject(reader);
                break;
            case "project-remove":
                projectController.removeProject(reader);
                break;
            case  "project-clear":
                projectController.clearProjects();
                break;
            case "task-create":
                taskController.createTask(reader);
                break;
            case "task-list":
                taskController.listTasks(reader);
                break;
            case "task-edit":
                taskController.editTask(reader);
                break;
            case "task-remove":
                taskController.removeTask(reader);
                break;
            case "task-clear":
                taskController.clearTask(reader);
                break;
        }
    }
}
