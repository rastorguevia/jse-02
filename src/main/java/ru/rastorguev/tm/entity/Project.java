package ru.rastorguev.tm.entity;

import java.util.LinkedList;
import java.util.List;

public class Project {
    private String name;
    private List<Task> tasks = new LinkedList<Task>();

    public Project(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Task> getTasks() {
        return tasks;
    }
}
